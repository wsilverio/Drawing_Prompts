#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include "prompts_header.h"
//#include "char_class.cpp"
//#include <stdio.h>      /* printf, fgets */
#include <stdlib.h>
using namespace std;

int k;
int rollDX (int x){
    srand (time(NULL)*k);
    return rand() % x + 1;
}

bool coinToss(){

    srand (time(NULL)*rollDX(rollDX(150)));
    int random = rand() % 10 + 1;
    if(random > 5){
        return true;
    }
    else{
        return false;
    }
}

bool coinToss(int p){
    srand (time(NULL)*rollDX(rollDX(150)));
    int random = rand() % 100 + 1;
    if(random < p){
        return true;
    }
    else{
        return false;
    }
}

vector<string> character_adjectives;
vector<string> adverbs;
vector<string> rideables;
vector<string> weapons;
vector<string> objects;
vector<string> colors;
vector<string> locations;
vector<string> times;
vector<string> pluralPronouns;

//class Character;

class Verb{
    public:string present, cj, p, ct;
    public:vector<string> particular_adverbs;
    public:vector<string> objects;
    public:vector<string> theme;
    public:bool transitive, advObligatory, objObligatory;
    Verb(string pres, string conj, string past, string cont){
        present = pres;
        cj = conj;
        p = past;
        ct = cont;
    }
    string conjugated(){
        if(cj[0] == '-'){
            string out = present;
            string add = cj;
            add.erase(0, 1);
            out += add;
            return out;
        }
        else{
            return cj;
        }
    }
    string past(){
        if(p[0] == '-'){
            string out = present;
            string add = p;
            add.erase(0, 1);
            out += add;
            return out;
        }
        else{
            return p;
        }
    }
    string continuous(){
        if(ct[0] == '-'){
            string out = present;
            string add = ct;
            add.erase(0, 1);
            out += add;
            return out;
        }
        else{
            return ct;
        }
    }
    string object(Character c){
        return filter(objects[rollDX(objects.size())-1], c);
    }
    string adverb(){
        if(coinToss()){
            return particular_adverbs[rollDX(particular_adverbs.size())-1];
        }
        else{
            return adverbs[rollDX(adverbs.size())-1];
        }
    }
};

class Noun{
    //public:bool ;
    public:string s, p;
    public:vector<string> t;
    public:vector<string> a;
    public:int g;
    public:int n;
    Noun(string singular, string plural, int gender, int number, vector<string> adjectives, vector<string> theme){
        s = singular;
        p = plural;
        g = gender;
        n = number;
        a = adjectives;
        t = theme;
    }
    string plural(){
        if(p[0] == '-'){
            string out = s;
            string add = p;
            add.erase(0, 1);
            out += add;
            return out;
        }
        else{
            return p;
        }
    }
};

bool vowelCheck(char a){
    if(a == 'a' || a == 'e' || a == 'i' || a == 'o' || a == 'u' || a == 'y'){
        return true;
    }else return false;
}

string doArticle(bool s, string next){
    string output;
    if(s){
        if(coinToss()){
            output += "the ";
        }else{
            if(vowelCheck(next[0])) output += "an ";
            else output += "a ";
        }
    }else{
        output += pluralPronouns[rollDX(pluralPronouns.size())-1] + " ";
    }
    return output;
}

vector<Verb> verbs;
vector<Noun> nouns;

Character::Character(){
            person = rollDX(3);
            gender  = rollDX(3);
            singular = coinToss();
            do_adjective = coinToss(60);
            noun_index = rollDX(nouns.size())-1;
            if(do_adjective) adjective_index = rollDX(character_adjectives.size())-1;
        }
string Character::produce(){
        //cout<<"shit's going down"<<endl;
        string output, adjective, noun;

        if(singular) noun = nouns[noun_index].s;
        else noun = nouns[noun_index].plural();

        if(do_adjective){
            adjective = character_adjectives[adjective_index];
            output += doArticle(singular, adjective) + adjective + " ";
        }
        else output += doArticle(singular, noun);

        output += noun + " ";
        return output;
    }

bool convert_to_bool(string s){
    if( s == "1" || s == "true") return 1;
    else if (s == "0" || s == "false") return 0;
}

vector<string> split(string in, char lim){
    vector<string> out;
    string temp = "";
    for(int i = 0; i < in.length(); i++){
        if(in[i] == lim){
            out.push_back(temp);
            temp.clear();
        }else{
            temp += in[i];
        }
    }
    out.push_back(temp);
    return out;
}

void loadFiles(){
    ifstream reader;
    string input;
    reader.open("character adjectives.txt");
    if (reader.is_open()){
        while ( getline (reader, input) ){
          character_adjectives.push_back (input);
          input.clear();
        }
    }
    reader.close();
    reader.open("adverbs.txt");
    if (reader.is_open()){
        while ( getline (reader, input) ){
          adverbs.push_back (input);
          input.clear();
        }
    }
    reader.close();
    reader.open("rideables.txt");
    if (reader.is_open()){
        while ( getline (reader, input) ){
          rideables.push_back (input);
          input.clear();
        }
    }
    reader.close();
    reader.open("weapons.txt");
    if (reader.is_open()){
        while ( getline (reader, input) ){
          weapons.push_back (input);
          input.clear();
        }
    }
    reader.close();
    reader.open("PluralPronouns.txt");
    if (reader.is_open()){
        while ( getline (reader, input) ){
          pluralPronouns.push_back (input);
          input.clear();
        }
    }
    reader.close();

    reader.open("nouns.txt");
    if (reader.is_open()){
        while ( getline (reader, input) ){
            vector<string> si;
            si = split(input, '\t');
            if(si.size() == 6){
                int gender, number;
                gender = atoi(si[2].c_str());
                number = atoi(si[3].c_str());
                Noun n = Noun( si[0], si[1], gender, number, split(si[4], ' '), split(si[5], ' '));
                nouns.push_back (n);
            }
            input.clear();
        }
    }
    reader.close();

    reader.open("verbs.txt");
    if (reader.is_open()){
        while ( getline(reader, input) ){
            vector<string> si;
            si = split(input, '\t');
            if(si.size() == 7){
                Verb v = Verb(si[0], si[1], si[2], si[3]);
                v.particular_adverbs = split(si[4], ' ');
                v.transitive = convert_to_bool(si[5]);
                v.objects = split(si[6], ' ');
                verbs.push_back(v);
            }
            input.clear();
        }
    }
    reader.close();

    reader.open("objects.txt");
    if (reader.is_open()){
        while ( getline (reader, input) ){
          objects.push_back (input);
          input.clear();
        }
    }
    reader.close();

    reader.open("colors.txt");
    if (reader.is_open()){
        while ( getline (reader, input) ){
          colors.push_back (input);
          input.clear();
        }
    }
    reader.close();

    reader.open("locations.txt");
    if (reader.is_open()){
        while ( getline (reader, input) ){
          locations.push_back (input);
          input.clear();
        }
    }
    reader.close();
    reader.open("times.txt");
    if (reader.is_open()){
        while ( getline (reader, input) ){
          times.push_back (input);
          input.clear();
        }
    }
    reader.close();
    /*
    reader.open(".txt");
    if (reader.is_open()){
        while ( getline (reader, input) ){
          .push_back (input);
          input.clear();
        }
    }
    reader.close();
    */
}

string do_random_Noun(bool s){
    string output, adjective, noun;
    bool doAdjective = coinToss(60);
    output = "";

    if(s) noun = nouns[rollDX(nouns.size())-1].s;
    else noun = nouns[rollDX(nouns.size())-1].plural();

    if(doAdjective){
        adjective = character_adjectives[rollDX(character_adjectives.size())-1];
        output += doArticle(s, adjective) + adjective + " ";
    }
    else output += doArticle(s, noun);

    output += noun + " ";
    return output;
}

string doObject(){
    string output, object;
    if( coinToss() ){
        //doArticle();
    }
    else{
       // PossessiveDeterminer();
    }
    return output;
}

Character subject, object;
bool already_objected;

string PersonalPronoun(Character c, bool subject){// singular as opposed to plural, subject as opposed to object
    switch(c.person){
        case 1:
            if( c.singular ){
                if( subject ){
                    return "I";
                }else{
                    return "me";
                }
            }
            else{
                if( subject ){
                    return "we";
                }else{
                    return "us";
                }
            }
            break;
        case 2:
            return "you";
            break;
        case 3:
            if( c.singular ){
                switch(c.gender){
                    case 1:
                        if( subject ){
                            return "he";
                        }else{
                            return "him";
                        }
                        break;
                    case 2:
                        if( subject ){
                            return "she";
                        }else{
                            return "her";
                        }
                        break;
                    case 3:
                        return "it";
                        break;
                }
            }
            else{
                if( subject ){
                    return "they";
                }else{
                   return "them";
                }
            }
            break;
    }
}

string PossessiveDeterminer(Character c){
    switch(c.person){
        case 1:
            if( c.singular ){
                return "my";
            }else{
                return "our";
            }
            break;
        case 2:
            return "your";
            break;
        case 3:
            if( c.singular ){
                switch(c.gender){
                    case 1:
                        return "his";
                        break;
                    case 2:
                        return "her";
                        break;
                    case 3:
                        return "its";
                        break;
                }
            }else{
                return "their";
            }
            break;
    }
}

string do_random_Verb(Character c){
    string output, verb;
    bool doAdverb;
    int verbID;
    doAdverb = coinToss(60);

    verbID = rollDX(verbs.size()) - 1;
    if(c.singular && c.person == 3)output += verbs[verbID].conjugated() +  " ";
    else output += verbs[verbID].present +  " ";

    output += verbs[verbID].object(c);

    if(doAdverb) output += verbs[verbID].adverb() +  " ";

    return output;
}

string doVerb(int id, Character c){
    string output, verb;
    bool doAdverb;
    doAdverb = coinToss(60);

    if(c.singular && c.person == 3)output += verbs[id].conjugated() +  " ";
    else output += verbs[id].present +  " ";

    output += verbs[id].object(c) + " ";

    if(doAdverb) output +=  verbs[id].adverb() +  " ";

    return output;
}

string filter(string s, Character c){
    string output = "";
    vector<string> spl;
    spl = split(s, '_');
    for(int i = 0; i < spl.size(); i++){
        string o = spl[i];
        if(o[0] == '#'){
            switch(o[1]){
            case 'v':
                do_random_Verb(c);
                break;
            case 'i':{
                Character k = Character();
                k.singular = true;
                k.person = 2;
                output += "to" + do_random_Verb(k);
                }
                break;
            case 'n':
                if(!already_objected){
                    if( coinToss(30)){
                        output += PersonalPronoun(c, false);
                    }
                    else output += c.produce();
                    already_objected = true;
                }
                break;
            case 'w':
                output += weapons[rollDX(weapons.size())-1];
                break;
            case 'o':
                output += objects[rollDX(objects.size())-1];
                break;
            case 'c':
                output += colors[rollDX(colors.size())-1];
                break;
            case 'l':
                output += locations[rollDX(locations.size())-1];
                break;
            case 't':
                output += times[rollDX(times.size())-1];
                break;
            case 'a':
                output += doArticle(coinToss(), "i don't fucking know");
                break;
            case 'd':
                output += PossessiveDeterminer(c);
                break;
            case 'p':
                output += PersonalPronoun(c, true);
                break;
            }
            output += " ";
        }
        else{
            output += spl[i]+" ";
        }
    }
    return output;
}

string Assembler(char o){
    bool place = false;
    bool time = false;
    string output = "";

    //if(o == 'r') o = rollDX(2);

    switch(o){
    case 's': //simple
        already_objected = false;
        subject = Character();
        object = Character();
        subject.person = 3;
        output = subject.produce() + do_random_Verb(subject);
        break;
    case 'b': //before
        subject = Character();
        object = Character();
        output = subject.produce() + do_random_Verb(subject) + "before " + PersonalPronoun(object, false) + do_random_Verb(object);
        break;
    //case 'e':

       // break;
    }

    bool notdone = true;
    for(int i = output.size()-1; i >= 0 && notdone; i--){
        if( output[i] == ' ') output.erase(i); //output.begin()+
        else notdone = false;
    }
    output += ".";
    return output;
}

/*
#v verb pres/conj
#i verb infinitive
#n noun
#w weapons
#o objects
#c colors
#l location
#t time
#a article
#d possessive determiner
#p personal pronoun
*/
int main(){
    bool loop;
    loadFiles();

    k = rollDX(rollDX(500));
    loop = 1;
    cout<<"Welcome to Introscopia's Drawing Prompts 1.0"<<endl<<endl;
    cout<<"select: [x]:Exit  [r]:Random  [s]:Simple  [b]:\"Before\""<<endl<<endl; //cout<<"[0]:Random   [1]:Short   [2]:Long    [3]:duel"<<endl<<">";
    while(loop){
        char option;
        cout<<">";
        cin>>option;
        cout<<endl;
        if(option == 'x') loop = 0;
        else cout<<Assembler(option)<<endl<<endl;

        k = rollDX(rollDX(500));
    }
}
