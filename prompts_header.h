#ifndef PROMPTS_HEADER_H_INCLUDED
#define PROMPTS_HEADER_H_INCLUDED

#include <string>
#pragma once

class Character{
    public:
        int noun_index, adjective_index;
        int person, gender;
        bool singular, do_adjective;
        Character();
        std::string produce();
};

std::string filter(std::string s, Character c);

int rollDX (int x);

bool coinToss();

bool coinToss(int p);


#endif // PROMPTS_HEADER_H_INCLUDED
